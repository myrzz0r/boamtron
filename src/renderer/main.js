import './bootstrap'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))

const {ipcRenderer} = require('electron')
ipcRenderer.on('asynchronous-reply', (event, arg) => {
 console.log(arg) // prints "pong"
})
ipcRenderer.send('asynchronous-message', 'ping')

import App from './App'
import router from './router'
import store from './store'
import './mixins/global'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
