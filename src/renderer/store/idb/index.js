import Dexie from 'dexie' // '@dpogue/dexie'

const db = new Dexie("BoambtronLocalDB");

db.version(1).stores({
	filters: "++id,tab,checked,ctrl,category1,category2,category3,min_lev,max_lev,grade,item_name,sort_type,sort_value,evolution_level,upgrade_level,page,lang,my_gold,world,csn,is_use,is_buy,is_favorite,is_csearch,wish_price"
});

export default db