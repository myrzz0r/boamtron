import db from "@/store/idb"

window.db = db

const DefaultFilter = {
  ctrl: 'list',
  category1: 9999,
  category2: 9999,
  category3: 9999,
  min_lev: '',
  max_lev: '',
  grade: 'all',
  item_name: '',
  sort_type: 3, // 3 - price
  sort_value: 2, // 1 - desc, 2 - asc
  evolution_level: -1,
  upgrade_level: -1,
  page: 1,
  lang: 'ENG',
  my_gold: 0,
  world: 0,
  csn: 0,
  is_use: false,
  is_buy: false,
  is_favorite: false,
  is_csearch: false, // true - полное совпадение имени
	wish_price: 0,
  tab: null,
  by_runes: [],
  id: 0,
}

const state = {
  list: [],
}

const getters = {
  filters: state => {
    return state.list
  },
  
  difference(state) {
    function changes(object, base) {
      return _.transform(object, function(result, value, key) {
        if (!_.isEqual(value, base[key])) {
          result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
        }
      });
    }

    if(state.list && state.list.length <= 0) return []; 
    return state.list.map(item => changes(item, DefaultFilter));   
  }
}

const mutations = {
  updateList(state, list) {
    state.list = list
  },

  addFilter(state, filter) {
    state.list.push(filter)
  },
}

const actions = {
  initFiltersList({ commit}) {
    db.filters.toArray().then(list => {
      commit('updateList', list)
    })
  },

  addFilter({ commit }, filter) {
    let item = { ...filter, checked: 0 }

    return db.filters.add(item).then(() => {
      commit('addFilter', item)
    })
  },

  updateFilter({ dispatch, commit }, { id, changes }) {
    db.filters.update(id, changes).then(() => dispatch('initFiltersList'))
  },

  removeFilter({ dispatch, commit }, id) {
    db.filters.delete(id).then(() => dispatch('initFiltersList'))
  },
  
  toggleChecked({ dispatch, commit }, [ids, force]) {
    let time = (new Date).getTime()
    db.filters.where(':id').anyOf(ids).modify(function(filter) {
      filter.checked = typeof(force) !== 'undefined' ? (force ? time : 0) : (filter.checked > 0 ? 0 : time)
    }).then(() => dispatch('initFiltersList'))
  },
}

export default {
  state,
  getters,
  mutations,
  actions
}
