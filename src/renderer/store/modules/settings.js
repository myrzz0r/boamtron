let savedSettings = localStorage.getItem('settings')

const defaultSettings = {
	curDate: new Date(),
	monitoring: {
		filter_sizes: {
			xs: 6,
			sm: 4,
			md: 4,
			lg: 3,
		}
	}
}

const state = savedSettings ? JSON.parse(savedSettings) : Object.assign({}, _.cloneDeep(defaultSettings))

const getters = {

}

const mutations = {
	updateTime(sate) {
		state.curDate = new Date()
	}
}

const actions = {

}

export default {
  state,
  getters,
  mutations,
  actions
}
