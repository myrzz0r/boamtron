import Vue from 'vue'
import Router from 'vue-router'

import FiltersPage from '@/components/FiltersPage'
import MonitorPage from '@/components/MonitorPage'
import SearchPage from '@/components/SearchPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'search-page',
      component: SearchPage
    },  
    {
      path: '/monitor',
      name: 'monitor-page',
      component: MonitorPage
    },
    {
      path: '/filters',
      name: 'filters-page',
      component: FiltersPage
    },  
    {
      path: '*',
      redirect: '/'
    }
  ]
})