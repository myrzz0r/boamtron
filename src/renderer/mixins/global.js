import notify1 from '@/assets/notifications/notify-1.mp3'
import notify2 from '@/assets/notifications/notify-2.mp3'

const audios = {
   notify1: new Audio(notify1),
   notify2: new Audio(notify2)
}

Vue.mixin({
	computed: {
		settings() {
			return this.$store.state.settings
		}
	},

   methods: {
      setRightDrawerOptions(options) {
         this.$root.$emit('setRightDrawerOptions', options);
      },

      toggleRightDrawer(force) {
         this.$root.$emit('toggleRightDrawer', force);
      },

      notify(text, color = null, timeout = 3000) {
         this.$root.$emit('showNotify', { text, color, timeout });         
      },

      sound(name) {
         audios[name].play()
      }
   },

   beforeRouteLeave (to, from, next) {
      this.toggleRightDrawer(false)

      next()
   }
})