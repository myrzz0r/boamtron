const grades = [
	{ value: 'all', text: 'All', color: '#fff' },
	{ value: 'poor', text: 'Faulty↑', color: '#7f7f7f' },
	{ value: 'common', text: 'Common↑', color: '#fff' },
	{ value: 'uncommon', text: 'Superior↑', color: '#80a94a' },
	{ value: 'rare', text: 'Rare↑', color: '#3479be' },
	{ value: 'epic', text: 'Heroic↑', color: '#686bd7' },
	{ value: 'legendary', text: 'Legendary↑', color: '#b37f47' },
];
const evolutions = [
	{ text: 'All', value: -1 },
	{ text: 'None', value: 0 },
	{ text: '1Level', value: 1 },
	{ text: '2Level', value: 2 },
	{ text: '3Level', value: 3 },
];
const enhancements = [
	{ text: 'All', value: -1 },
	{ text: '+1 Over', value: 1 },
	{ text: '+2 Over', value: 2 },
	{ text: '+3 Over', value: 3 },
	{ text: '+4 Over', value: 4 },
	{ text: '+5 Over', value: 5 },
	{ text: '+6 Over', value: 6 },
	{ text: '+7 Over', value: 7 },
	{ text: '+8 Over', value: 8 },
	{ text: '+9 Over', value: 9 },
	{ text: '+10 Over', value: 10 },
	{ text: '+11 Over', value: 11 },
	{ text: '+12 Over', value: 12 },
	{ text: '+13 Over', value: 13 },
	{ text: '+14 Over', value: 14 },
	{ text: '+15 Over', value: 15 },
];

const categories = [
	{
		value: 9999,
		text: 'All'
	},
	{
		value: 1,
		text: 'Weapon',
		subcategories: [
			{
				value: 1,
				text: 'Main Weapon',
				subcategories: [
					{ value: 1, text: 'Dagger' },
					{ value: 2, text: 'One-handed Sword' },
					{ value: 3, text: 'One-handed Blunt Weapon' },
					{ value: 4, text: 'One-handed Hammer' },
				]
			},
			{
				value: 2,
				text: 'Side Weapon',
				subcategories: [
					{ value: 1, text: 'Shield' },
				]
			},
			{
				value: 3,
				text: 'Two-handed Weapon',
				subcategories: [
					{ value: 1, text: 'Two-handed Hammer' },
					{ value: 2, text: 'Two-handed Axe' },
					{ value: 3, text: 'Two-handed Blunt Weapon' },
					{ value: 4, text: 'Bow' },
					{ value: 5, text: 'Staff' },
					{ value: 6, text: 'Wand' },
				]
			},
		]
	}, // 
	{
		value: 2,
		text: 'Armor',
		subcategories: [
			{
				text: 'Head',
				value: 1,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
			{
				text: 'Shoulder',
				value: 2,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
			{
				text: 'Chest',
				value: 3,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
			{
				text: 'Waist',
				value: 4,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
			{
				text: 'Hand',
				value: 5,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
			{
				text: 'Leg',
				value: 6,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
			{
				text: 'Foot',
				value: 7,
				subcategories: [
					{ value: 1, text: 'Robe' },
					{ value: 2, text: 'Light Armor' },
					{ value: 3, text: 'Medium Armor' },
					{ value: 4, text: 'Heavy Armor' },
				]
			},
		]
	}, // 
	{
		value: 3,
		text: 'Accessory',
		subcategories: [
			{ value: 1, text: 'Finger' },
			{ value: 2, text: 'Neck' },
			{ value: 3, text: 'Wrist' },
		]
	},
	{
		value: 4,
		text: 'Item',
		subcategories: [
			{ value: 2, text: 'Health Recovery Potion' },
			{ value: 3, text: 'Energy Recovery Potion' },
			{ value: 4, text: 'Health and Energy Recovery Potion' },
			{ value: 5, text: 'Production tools' },
			{ value: 6, text: 'Craft Material' },
			{ value: 7, text: 'Food' },
			{ value: 8, text: 'Battle Item' },
			{ value: 9, text: 'How to craft' },
			{ value: 10, text: 'Portable craft table' },
			{ value: 11, text: 'Repair Tool' },
			{ value: 12, text: 'Upgrade Armor, Enhancement Materials' },
		]
	},
	{
		value: 5,
		text: 'Companion',
		subcategories: [
			{ value: 5, text: 'Anima' },
			{ value: 8, text: 'Enhance Sub-Material' },
			{ value: 9, text: 'Upgrade Sub-Material' },
			{ value: 10, text: 'Companion Skill Change Token' },
		]
	},
	{
		value: 9,
		text: 'Spirit',
		subcategories: [
			{ value: 1, text: 'Flying Dragon Spirit' },
			{ value: 2, text: 'Bear Spirit' },
			{ value: 3, text: 'Eagle Spirit' },
			{ value: 4, text: 'Lion Spirit' },
			{ value: 5, text: 'Leopard Spirit' },
			{ value: 6, text: 'Wolf Spirit' },
			{ value: 7, text: 'Crow Spirit' },
			{ value: 8, text: 'Hawk Spirit' },
			{ value: 9, text: 'Owl Spirit' },
			{ value: 10, text: 'Wind Spirit' },
			{ value: 11, text: 'Mist Spirit' },
			{ value: 12, text: 'Waterfall Spirit' },
			{ value: 13, text: 'Rock Spirit' },
			{ value: 14, text: 'Old Tree Spirit' },
			{ value: 15, text: 'Sea Spirit' },
		]
	},
	{
		value: 8,
		text: 'Supplies',
	},
];

const rune_list = [
	{ value: 'Dragon', text: 'Flying Dragon Spirit' },
	{ value: 'Bear', text: 'Bear Spirit' },
	{ value: 'Eagle', text: 'Eagle Spirit' },
	{ value: 'Lion', text: 'Lion Spirit' },
	{ value: 'Leopard', text: 'Leopard Spirit' },
	{ value: 'Wolf', text: 'Wolf Spirit' },
	{ value: 'Raven', text: 'Crow Spirit' },
	{ value: 'Hawk', text: 'Hawk Spirit' },
	{ value: 'Owl', text: 'Owl Spirit' },
	{ value: 'Wind', text: 'Wind Spirit' },
	{ value: 'Fog', text: 'Mist Spirit' },
	{ value: 'Waterfall', text: 'Waterfall Spirit' },
	{ value: 'Rock', text: 'Rock Spirit' },
	{ value: 'Tree', text: 'Old Tree Spirit' },
	{ value: 'Sea', text: 'Sea Spirit' },
];


export default {
	data: () => ({
		grades,
		evolutions,
		enhancements,
		categories,
		rune_list
	}),

	methods: {
		getFilterName(item) {
			let catName = this.getCategory(item)
			if(catName.length > 0) {
				return item.item_name && item.item_name.length > 0 ? item.item_name : catName
			}

			return item.item_name && item.item_name.length > 0 ? item.item_name : 'NoNameFilter'
		},

		getCategory(item) {
			let label = '',
				category = null,
				category2 = null,
				category3 = null;

				if(item.category1) {
					category = _.find(this.categories, { value: item.category1 })
				}

				if(category && category.subcategories && item.category2) {
					category2 = _.find(category.subcategories, { value: item.category2 })
				}

				if(category2 && category2.subcategories && item.category3) {
					category3 = _.find(category2.subcategories, { value: item.category3 })
				}

				if(category) {
					label = category.text
				}
				if(category2) {
					label += ` > ${category2.text}`
				}
				if(category3) {
					label += ` > ${category3.text}`
				}

				return label
		},
	}

}