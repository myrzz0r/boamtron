
window._ = require('lodash');

import 'babel-polyfill'
import Vue from 'vue'
window.Vue = Vue;

// Vue.config.silent = true

import Vuetify from 'vuetify'
Vue.use(Vuetify)

import PortalVue from 'portal-vue'
Vue.use(PortalVue)

import axios from 'axios'
window.axios = axios

/*
window.axios.defaults.baseURL = '/api';
window.axios.defaults.headers.common = {
   'X-Requested-With': 'XMLHttpRequest'
};
*/