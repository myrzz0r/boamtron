import { ipcMain } from 'electron'
import request from 'request-promise-native'
import cheerio from 'cheerio'
import queue from 'async/queue'
import _ from 'lodash'

const GRADES = [
   { value: 'all', label: 'All' },
   { value: 'poor', label: 'Faulty↑' },
   { value: 'common', label: 'Common↑' },
   { value: 'uncommon', label: 'Superior↑' },
   { value: 'rare', label: 'Rare↑' },
   { value: 'epic', label: 'Heroic↑' },
   { value: 'legendary', label: 'Legendary↑' },
];

const getList = function(post_data) {
   return request({
      method: 'POST',
      uri: 'http://wic-ff-lb.blessonline.net/bless/exchange/index.php',
      form: post_data,
      transform: function (body) {
      return cheerio.load(body);
   }
   })
}

const calcPrice = function($, prices) {
   var price = {}

   prices.each(function() {
      var cPrice = $(this),
         values = cPrice.find('span'),
         type = cPrice.find('em').text().toLowerCase().replace(/\s+/g, ''),
         value = values.map(function() {
            var v = parseInt($(this).text().replace(/[^0-9]+/g, ''));
            if($(this).hasClass('g'))
               return v;
            if(v == 0)
               return '00';
            if(v < 10)
               return `0${v}`;
            return v;
         }).get().join('').replace(/^[0]+/, '');

      price[type] = parseInt(value);
   });

   return price
}

class Attributes {
   constructor(node) {
      this.attrs = {}
      this.parse(node)
   }

   get(name) {
      if(name == 'rune_slots') {
         let o = _.find(this.attrs, { name: 'socket_info' }),
            RegEx = /RunicAlphabet_([a-zA-Z]+)[_\.]/g,
            matches = null
         if(o && o.value.length > 0) {
            matches = o.value.match(RegEx)            
         }
         return matches ? matches.map(text => text.replace(RegEx, "$1")) : null
      }

      let o = _.find(this.attrs, { name })
      return o ? o.value : null
   }

   parse(node) {
      this.attrs = _.map(node.attribs, (value, name) => {
         if(name == 'tool_tip' && value.length > 0) {
            value = _.values(JSON.parse(value))
         }         

         if(['upgrade_level', 'max_potential_lv', 'item_grade', 'item_min_lev', 'icon_index'].includes(name)) {
            value = parseInt(value)
         }

         return { name, value };
      });
   }
}

const parseList = function($) {
   if($('.wt_table .no_result').length > 0) return [];

   let list = []

   $('.wt_table table tbody tr').each(function(i, o) {
      let buyBtn = $(this).find('.col_btn a'),
         btnGraph = $(this).find('.col_graph p a'),
         itemInfo = $(this).find('.wtitem_holder .item_thumb data'),
         prices = calcPrice($, $(this).find('td.col_price .money_holder p')),
         attrs = new Attributes(itemInfo.get(0)),
         item_grade = attrs.get('item_grade') + 1,
         item = {
            uid: parseInt(btnGraph.attr('onclick').replace(/.+\('([^']+)',.+/, '$1')),
            img: $(this).find('.wtitem_holder .item_thumb img').attr('src'),
            count: parseInt($(this).find('.wtitem_holder .item_thumb > i').text()),
            item_level: attrs.get('item_min_lev'),
            name: attrs.get('item_name'),
            type: attrs.get('item_category_name'),
            enhancement: attrs.get('upgrade_level'),
            socket_info: attrs.get('socket_info'),
            rune_slots: attrs.get('rune_slots'),
            tootltip: attrs.get('tool_tip'),
            grade: item_grade,
            gradeName: GRADES[item_grade] ? GRADES[item_grade].value : null,
            priceEach: prices.each,
            priceTotal: prices.total,           
            btn: {
               type: buyBtn.text(),
               action: buyBtn.attr('onclick')
            }
         }

      list.push(item)
   });

   return list;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const getPage = (filter) => {
   return getList(filter).then($ => {
      return parseList($).map(el => {
         el.auctionPage = filter.page
         return el
      });      
   }).catch(console.error);
}

const getTotalPages = (data) => {
   let lastPage = 0

   return getList(data)
      .then($ => {
         if($('.wt_table .no_result').length > 0)
            return lastPage

         let formData = _.clone(data),
            prev = $('.wt_paging .prev a').attr('href'),
            next = $('.wt_paging .next a').attr('href'),
            nums = $('.wt_paging .num a').map(function() {
               return parseInt($(this).text())
            });

         lastPage = _.max(nums);
         if(next) {
            formData.page = parseInt(next.replace(/[^0-9]+/g, ''))
            return formData
         }
         return lastPage         
      }).then(res => {
         if(res && _.isObject(res))
            return getTotalPages(res)
         return res
      }).catch(console.error);
}

const getAllLists = (data) => {
   return getTotalPages(data)
      .then(lastPage => {
         if(!lastPage) return []

         return Promise.all(
            _.range(1, ++lastPage).map(page => getPage({ ...data, page }))
         ).then(allLists => {
            return _.orderBy(
               _.flatten(allLists),
               ['priceEach'],
               [(data.sort_value == 1 ? 'desc' : 'asc')]
            )
         }).catch(console.error)
      }).catch(console.error)
}

const startMonitoring = () => {
   return setInterval(() => {
      if(sender && selectedFilters.length) {
         processAllFilteres(selectedFilters)
         .then(collection => {
            let list = {}
            _.each(collection, o => {
               list[o.id] = o.list
            })
            return list
         }).then(list => {
            sender.send('getAllFiltersResponse', list);
            
         }).catch(console.error)
      }
   }, 5000)
}

const processFilter = async (filter) => {
   let list = [];

   if(filter.by_runes && filter.by_runes.length) {
      list = await getAllLists(filter)
      list = list.filter(e => _.intersection(e.rune_slots, filter.by_runes).length > 0)
      list = _.take(list, 6)
   } else {
      list = await getPage(filter)
   }

   return Promise.resolve(list)
}

const processAllFilteres = (filteres) => {
   return Promise.all(filteres.map(async f => {
      let list = await processFilter(f)
      return { id: f.id, list }
   }))
}



let selectedFilters = [],
   sender = null,
   timer = null


ipcMain.on('stopMonitoring', (event, filter) => {
   if(timer) clearInterval(timer)
   timer = null
})

ipcMain.on('setSelectedFilters', (event, list) => {
   if(list) {
      selectedFilters = list
      sender = event.sender
      if(!timer)
         timer = startMonitoring()
   }
})

ipcMain.on('getPageRequest', (event, filter) => {
   getPage(filter).then(list => {
      event.sender.send('getPageResponse', { id: filter.id, list });
   })
})

ipcMain.on('getAllPagesRequest', (event, filter) => {   
   getAllLists(filter).then(list => {
      event.sender.send('getAllPagesResponse', list);
   })
})